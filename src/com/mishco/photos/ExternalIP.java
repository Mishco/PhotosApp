package com.mishco.photos;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.os.AsyncTask;
import android.util.Log;

class ExternalIP extends AsyncTask<Void,Void,String> {

	protected String doInBackground(Void... urls) {
        String ip = "Empty";

        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httpget = new HttpGet("http://checkip.amazonaws.com");
            HttpResponse response;

            response = httpclient.execute(httpget);

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                long len = entity.getContentLength();
                if (len != -1 && len < 1024) {
                    String str = EntityUtils.toString(entity);
                    ip = str.replace("\n", "");
                } else {
                    ip = "Response too long or error.";
                }
            } else {
                ip = "Null:" + response.getStatusLine().toString();
            }

        } catch (Exception e) {
            ip = "Error " + e.toString();
        }

        return ip;
    }

    protected void onPostExecute(String result) {

        // External IP 
        Log.d("INF>", result);
    }
}
