package com.mishco.photos;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class MainActivity extends Activity implements SensorEventListener {

	/*
	 * Cursor used to access the results from querying for images on the SD
	 * card.
	 */
	// private Cursor cursor;
	// /*
	// * Column index for the Thumbnails Image IDs.
	// */
	// private int columnIndex;
	//
	//
	/*
	 * For remote connection to server
	 */
	String apiMethod = "https://";
	String apiVersion = "/v22";
	String apiServer = "api.weaved.com";
	String apiKey = "WeavedDemoKey$2015";
	private final String USER_AGENT = "Mozilla/5.0";
	protected String token = "";
	// Internat http
	String UID = "80:00:00:05:46:01:B3:E6";
	// Internat ssh
	String UID2 = "80:00:00:05:46:01:B3:E7";

	private boolean isHalStartingOn;
	ImageButton btnSwitch;

	ImageView imgFullImage;
	private SensorManager mSensorManager;
	private Sensor mAccelerometer;
	private Sensor mGyroscope;

	private FileWriter writer;
	private String TAG = "INF>";
	FileOutputStream acceFos;
	FileOutputStream gyroFos;
	File filenew;
	long startTime;

	String FILENAME_ACCELEROMETER = "accedata.txt";
	String FILENAME_GYROSCOPE = "gyrodata.txt";

	/*
	 * check connection to internet Wifi or Data
	 */
	protected void createConnection() {
		if (isOnline()) {
			try {
				sendGet();
				Toast.makeText(MainActivity.this, "Success", Toast.LENGTH_SHORT)
						.show();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else
			Toast.makeText(MainActivity.this, "No connection to internet",
					Toast.LENGTH_LONG).show();

		Log.d("TOKEN", token);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		btnSwitch = (ImageButton) findViewById(R.id.btnSwitch);
		isHalStartingOn = false;

		// NOT RECOMMENDED
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);

		createConnection();

		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mAccelerometer = mSensorManager
				.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		mGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

	}

	public void onStartClick(View view) {

		btnSwitch.setImageResource(R.drawable.halon);
		isHalStartingOn = true;

		Log.d(TAG, "On start click");

//		Log.i(TAG, "ManuFacturer :"+ Build.MANUFACTURER);
//		Log.i(TAG, "Board : "+ Build.BOARD);
//		Log.i(TAG, "Display : " + Build.DISPLAY);
//		Log.i(TAG, "SERIAL: " + Build.SERIAL);
//		Log.i(TAG, "MODEL: " + Build.MODEL);
//		Log.i(TAG, "ID: " + Build.ID);
//		Log.i(TAG, "Manufacture: " + Build.MANUFACTURER);
//		Log.i(TAG, "brand: " + Build.BRAND);
//		Log.i(TAG, "type: " + Build.TYPE);
//		Log.i(TAG, "user: " + Build.USER);
//		Log.i(TAG, "BASE: " + Build.VERSION_CODES.BASE);
//		Log.i(TAG, "INCREMENTAL " + Build.VERSION.INCREMENTAL);
//		Log.i(TAG, "SDK  " + Build.VERSION.SDK);
//		Log.i(TAG, "BOARD: " + Build.BOARD);
//		Log.i(TAG, "BRAND " + Build.BRAND);
//		Log.i(TAG, "HOST " + Build.HOST);
//		Log.i(TAG, "FINGERPRINT: " + Build.FINGERPRINT);
		Log.i(TAG, "Version Code: " + Build.VERSION.RELEASE);
		
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		Log.i(TAG, "Width: " + size.x + " Height: " + size.y);
	    
		List<Sensor> sensor = mSensorManager.getSensorList(Sensor.TYPE_ALL);
		Log.i(TAG, "Sensors: " + sensor);
		
		

		mSensorManager.registerListener(this, mAccelerometer,
				SensorManager.SENSOR_DELAY_FASTEST);
		mSensorManager.registerListener(this, mGyroscope,
				SensorManager.SENSOR_DELAY_FASTEST);
		startTime = System.currentTimeMillis();
	}

	public void onStopClick(View view) throws IOException {
		Log.d(TAG, "On stop click");
		mSensorManager.unregisterListener(this);
		btnSwitch.setImageResource(R.drawable.haloff);
		isHalStartingOn = false;

		String destUrl = "";
		// connect to specific device - raspberry pi
		try {
			destUrl = proxyConnect(UID, token);

			// Reconnect to proxy web page
			Uri uri = Uri.parse(destUrl);

			// Open new page
			Intent intent = new Intent(Intent.ACTION_VIEW, uri);
			startActivity(intent);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void onResume() {
		// String FILENAME = "sensordata.txt";
		super.onResume();
		try {
			// Creating new file in internal storage
			acceFos = openFileOutput(FILENAME_ACCELEROMETER,
					Context.MODE_PRIVATE);
			Log.d(TAG, acceFos.toString());
			gyroFos = openFileOutput(FILENAME_GYROSCOPE, Context.MODE_PRIVATE);

			// Adding head of file due to
			// more sensor similar data
			String buffer = "T, AccX, AccY, AccZ" + "\n";
			acceFos.write(buffer.getBytes());

			buffer = "T, Gx, Gy, Gz" + "\n";
			gyroFos.write(buffer.getBytes());

			// Get path for log
			String path = getFilesDir().getAbsolutePath();
			Log.d(TAG, path);
		} catch (IOException e) {
			e.printStackTrace();
			Log.e(TAG, e.toString());
		}
	}

	protected void onPause() {
		super.onPause();

		if (writer != null) {
			try {
				Log.d(TAG, writer.toString());
				acceFos.close();
				gyroFos.close();
			} catch (IOException e) {
				e.printStackTrace();
				Log.e(TAG, e.toString());
			}
		}

	}

	protected File saveDataToCsvFile(String DataFileName, String resultFileName) {

		String path = getFilesDir().getAbsolutePath() + '/' + DataFileName;

		Log.d(TAG, path);
		File file = new File(path);

		// Check if file exists
		if (file.exists()) {

			Log.d(TAG, "DATA EXISTS");
			String root = Environment.getExternalStorageDirectory().toString();
			File myDir = new File(root + "/saved_data");
			if (!myDir.exists())
				myDir.mkdirs();

			// Add timestamp before name of file
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
					"dd-MM-yyyy-hh-mm-ss");
			String format = simpleDateFormat.format(new Date());
			filenew = new File(myDir, format + "-" + resultFileName + ".csv");
			try {
				// copy from internal to external storage
				copyFile(file, filenew);
				// TO DO : send data to server
				Log.d(TAG, "Succesfull send data");
			} catch (IOException e) {
				Log.e(TAG, e.toString());
				e.printStackTrace();
			}
		} else {
			Log.e(TAG, "DATA DOES NOT EXIST");
		}
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
			final Intent scanIntent = new Intent(
					Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
			final Uri contentUri = Uri.fromFile(filenew);
			scanIntent.setData(contentUri);
			sendBroadcast(scanIntent);
		} else {
			final Intent intent = new Intent(Intent.ACTION_MEDIA_MOUNTED,
					Uri.parse("file://"
							+ Environment.getExternalStorageDirectory()));
			sendBroadcast(intent);
		}
		return filenew;

	}

	protected void onStop() {
		super.onStop();

		// copy from internal to external storage
		File storeFile = saveDataToCsvFile(FILENAME_ACCELEROMETER, "acceData");
		File storeFile2 = saveDataToCsvFile(FILENAME_GYROSCOPE, "gyroData");

		String destUrl = "";
		// connect to specific device - raspberry pi
		try {
			destUrl = proxyConnect(UID, token);

			// Reconnect to proxy web page
			Uri uri = Uri.parse(destUrl);

			// Author
			// https://stackoverflow.com/questions/25398200/uploading-file-in-php-server-from-android-device
			new UploadFileAsync().execute(storeFile.getAbsolutePath(), uri
					+ "/upload.php?filename=" + storeFile.getName());
			new UploadFileAsync().execute(storeFile2.getAbsolutePath(), uri
					+ "/upload.php?filename=" + storeFile2.getName());

			// TODO remove old .csv files

			// Open new page
			// Intent intent = new Intent(Intent.ACTION_VIEW, uri);
			// startActivity(intent);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("resource")
	public static void copyFile(File src, File dst) throws IOException {
		FileChannel inChannel = new FileInputStream(src).getChannel();
		FileChannel outChannel = new FileOutputStream(dst).getChannel();
		try {
			inChannel.transferTo(0, inChannel.size(), outChannel);
		} finally {
			if (inChannel != null)
				inChannel.close();
			if (outChannel != null)
				outChannel.close();
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {

	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		String tmp;
		Sensor sensor = event.sensor;
		float x = 0, y = 0, z = 0;
		float gx = 0, gy = 0, gz = 0;
		long currentTime = System.currentTimeMillis();
		if (currentTime >= startTime) {
			try {

				if (sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
					x = event.values[0];
					y = event.values[1];
					z = event.values[2];

					tmp = (currentTime - startTime) + "," + x + "," + y + ","
							+ z + "\n";
					acceFos.write(tmp.getBytes());

				} else if (sensor.getType() == Sensor.TYPE_GYROSCOPE) {
					gx = event.values[0];
					gy = event.values[1];
					gz = event.values[2];

					tmp = (currentTime - startTime) + "," + gx + "," + gy + ","
							+ gz + "\n";
					gyroFos.write(tmp.getBytes());
				}

			} catch (IOException e) {
				e.printStackTrace();
				Log.e(TAG, e.toString());
			}

		}
	}

	/*
	 * Method for check if device has Wifi on or other Internet connection
	 */
	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(MainActivity.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		return netInfo != null && netInfo.isConnectedOrConnecting();
	}

	// HTTP GET request
	private void sendGet() throws Exception {
		String content_type_header = "application/json";

		String userName = "mishcos@gmail.com"; // input("User name:")
		String password = "internat123"; // input("Password:")

		String url = new StringBuilder(apiMethod).append(apiServer)
				.append(apiVersion).append("/api/user/login/").append(userName)
				.append("/").append(password).toString();

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		// add request header
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Content-Type", content_type_header);
		con.setRequestProperty("apikey", apiKey);

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(
				con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		// print result
		System.out.println(response.toString());

		// Get token from json response
		try {
			JSONObject object = (JSONObject) new JSONTokener(
					response.toString()).nextValue();
			token = object.getString("token");
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	protected String proxyConnect(String UID, String token) throws IOException {
		String content_type_header = "application/json";
		String my_ip = "";

		// get public ip address
		ExternalIP exIP = new ExternalIP();
		my_ip = exIP.doInBackground();

		String url = new StringBuilder(apiMethod).append(apiServer)
				.append(apiVersion).append("/api/device/connect").toString();

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("POST");

		// add request header
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Content-Type", content_type_header);
		con.setRequestProperty("apikey", apiKey);
		con.setRequestProperty("token", token);

		// add request body
		String urlParameters = new StringBuilder()
				.append("{\"deviceaddress\":\"").append(UID)
				.append("\",\"hostip\":\"").append(my_ip)
				.append("\",\"wait\":\"true\"}").toString();

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(
				con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		// print result
		System.out.println(response.toString());

		String destinationUrl = "";
		// Get status from json response
		try {
			JSONObject object = (JSONObject) new JSONTokener(
					response.toString()).nextValue();

			if (object.getString("status").equals("true")) {
				JSONObject arr = object.getJSONObject("connection");
				destinationUrl = arr.getString("proxy");
				System.out.println(destinationUrl);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return destinationUrl;
	}

	// @SuppressWarnings("deprecation")
	// private void appendToFile(String str) {
	// try {
	//
	// File file = getFileStreamPath("test.txt");
	//
	// if (!file.exists()) {
	// Log.d("INF>",
	// "appendToFile: created file=" + file.getAbsolutePath());
	// file.createNewFile();
	// } else {
	// Log.d("INF>",
	// "appendToFile: exits file=" + file.getAbsolutePath());
	// }
	// FileOutputStream writer;
	// writer = openFileOutput(file.getName(), MODE_APPEND
	// | MODE_WORLD_READABLE);
	//
	// writer.write(str.getBytes());
	// writer.flush();
	// writer.close();
	//
	// } catch (FileNotFoundException e) {
	// e.printStackTrace();
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// }

	// private void readFromFile() {
	// FileInputStream fis = null;
	//
	// try {
	// File file = getFileStreamPath("test.txt");
	// fis = new FileInputStream(file);
	//
	// Log.d("INF>",
	// "Total file size to read (in bytes) : " + fis.available());
	// Log.d("INF>", "readFromFile: Path file=" + file.getAbsolutePath());
	//
	// int content;
	// StringBuilder sb = new StringBuilder();
	// while ((content = fis.read()) != -1) {
	// // convert to char and display it
	// sb.append((char) content);
	// }
	// Log.d("INF>", sb.toString());
	//
	// } catch (IOException e) {
	// e.printStackTrace();
	// Log.e("INF>", e.toString());
	// } finally {
	// try {
	// if (fis != null)
	// fis.close();
	// } catch (IOException ex) {
	// ex.printStackTrace();
	// Log.e("INF>", ex.toString());
	// }
	// }
	// }

	// private Bitmap decodeImage(String data) {
	// byte[] b = Base64.decode(data, Base64.DEFAULT);
	// Bitmap bmp = BitmapFactory.decodeByteArray(b, 0, b.length);
	// return bmp;
	// }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Adapter for our image files.
	 */
	// private class ImageAdapter extends BaseAdapter {
	// private Context context;
	//
	// public ImageAdapter(Context localContext) {
	// context = localContext;
	// }
	//
	// public int getCount() {
	// return cursor.getCount();
	// }
	//
	// public Object getItem(int position) {
	// return position;
	// }
	//
	// public long getItemId(int position) {
	// return position;
	// }
	//
	// public View getView(int position, View convertView, ViewGroup parent) {
	// ImageView picturesView;
	// if (convertView == null) {
	// picturesView = new ImageView(context);
	// // Move cursor to current position
	// cursor.moveToPosition(position);
	// // Get the current value for the requested column
	// int imageID = cursor.getInt(columnIndex);
	// // Set the content of the image based on the provided URI
	// picturesView.setImageURI(Uri.withAppendedPath(
	// MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, ""
	// + imageID));
	// picturesView.setScaleType(ImageView.ScaleType.FIT_CENTER);
	// picturesView.setPadding(8, 8, 8, 8);
	// picturesView
	// .setLayoutParams(new GridView.LayoutParams(100, 100));
	// } else {
	// picturesView = (ImageView) convertView;
	// }
	// return picturesView;
	// }
	// }
}
